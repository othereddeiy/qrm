import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import scipy.stats as stats
from colorama import init
from termcolor import colored
from halo import Halo
from factor_analyzer import FactorAnalyzer
from factor_analyzer.factor_analyzer import calculate_bartlett_sphericity
from FactorAnalysis import check_correlation_kmo_bartlett, get_Eigenvalues, tabulate_Eigenvalues, plot_Eigenvalues, analyze, get_classed_Eigenvalues
from tabulate import tabulate
'''
This file contains utility functions for the Factor Analysis programm
'''
def colorDependingOnValue(value: float, isHighestValue: bool = False):
    '''
    Adds beautiful colors to the passed value (float) in an interval of 0.25
    It returns a string
    isHighestValue is usually false, pass true to have a green string returned
    '''
    if isHighestValue:
        return colored(str(round(value, 7)), "green")
    else:
        if value > 0.25 and value < 0.5:
            return colored(str(round(value, 7)), "magenta")
        elif value > 0.5 and value < 0.75:
            return colored(str(round(value, 7)), "blue")
        elif value < 0.25:
            return colored(str(round(value, 7)), "red")
        elif value > 0.75 and value < 1:
            return colored(str(round(value, 7)), "cyan")
        else: 
            return colored(str(round(value, 7)), "yellow")
def convertToColouredList(loadings: list):
    '''
    This function stringifies the loading matrix and adds beautiful colors to the values depending on their value
    '''
    coloured_list = []
    for row in loadings:
        coloured_row = []
        for counter, value in enumerate(row):
            isHighest = isHighestValue(row, counter)
            coloured_row.append(colorDependingOnValue(value, isHighest))
        coloured_list.append(coloured_row)
    return coloured_list
def isHighestValue(row, index): 
    if row[index] == max(row, key=abs):
        return True
    else:
        return False
def colorHighestValue(coloured_list: list):
    coloured_list = list(coloured_list)
    coloured_list = [list(n) for n in coloured_list]
    for row in coloured_list: 
        print(row)
        max_val = max(row)
        max_index = row.index(max_val)
        print(max_index)
def colour_anti_image_corr(msa):
    '''
    Receives the MSA-Values of the anti image correlation matrix and returns a coloured list containing
    the values become green if they are greater than 0.6
    '''
    coloured_list = []
    for val in msa:
        if val > 0.6:
            coloured_list.append(colored(str(round(val, 4)), "green"))
        else:
            coloured_list.append(colored(str(round(val, 4)), "red"))
    return coloured_list

def addIndexToColouredList(coloured_list: list, indexes: list):
    '''
    Receives a list of lists and attaches a header item at the top of each list of lists for a header row to be tabulated later
    '''
    coloured_list = list(coloured_list)
    coloured_list = [list(n) for n in coloured_list]
    for counter, index in enumerate(indexes):
        coloured_list[counter].insert(0, index)
    return coloured_list

def getHeadersDependingOnFactors(number_of_factors: int):
    headers = []
    for i in range(number_of_factors):
        headers.append("Factor " + str(i + 1))
    return headers