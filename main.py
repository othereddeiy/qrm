import pandas as pd
from tabulate import tabulate
from FactorAnalysis import check_correlation_kmo_bartlett, get_Eigenvalues, tabulate_Eigenvalues, plot_Eigenvalues, analyze, get_classed_Eigenvalues, get_Factor_Scores
from colorama import init
from termcolor import colored
from util.util import convertToColouredList, addIndexToColouredList, getHeadersDependingOnFactors, colour_anti_image_corr
init()
#Prepare the dataset
#ENVIRONMENT VARIABLES => Necessary for the M in M*N of the used dataframe [df], the path of the dataset, etc. 
#This has to be entered depending on the name of the dataset in use
PATH_TO_DATA = 'data-final.csv'
#Last survey question for the big five personality test  => OPN10
INDEX_OF_LAST_COLUMN = 50 
'''
Preparing the dataset:
- Dropping N/A
- Dropping non numeric data
- Dropping columns that are not neccessary for the analysis
'''
df = pd.read_csv(PATH_TO_DATA, delimiter="\t")
df.select_dtypes(include='number')
df.dropna(inplace=True)
df = df.iloc[:, : INDEX_OF_LAST_COLUMN]
'''
Evaluating the Suitability of the dataset based:
We calculate:
- KMO
- Bartlett's test
- Correlation Matrix
- Each individual MSA value <=> Diagonal of the anti image correlation matrix
We plot: 
- Each correlation coefficient within a classed bar chart
'''
checked_values = check_correlation_kmo_bartlett(df, INDEX_OF_LAST_COLUMN)
#kmo_all => The calculated kmo value for the whoile dataset
kmo_all = checked_values[1]
#kmo_item => The calculated kmo value for each item in the dataset
kmo_item = list(checked_values[0])
#Convert to colored list
coloured_kmo = colour_anti_image_corr(kmo_item)
#Convert to coloured dataframe for easier tabulation
df_kmo = pd.DataFrame(coloured_kmo)
#Add index to the dataframe
df_kmo.insert(loc=0,
          column='Item',
          value=list(df))
print(colored("MSA values for each Item:", 'cyan'))
print(tabulate(df_kmo, tablefmt='psql', headers=['Item', 'MSA'], showindex=False))
'''
Extracting the Factors and their quantity.
We calculate:
- Eigenvalues
We plot:
- Screeplot (Eigenvalues)
'''
#Calculate the Eigenvalues
ev = get_Eigenvalues(df)
#Group them depending on whether they cross the 1 threshold or not
classed_eigenvalues = get_classed_Eigenvalues(ev)
#Print them and add some colour 
tabulate_Eigenvalues(ev)
#Plot the screeplot
plot_Eigenvalues(ev)
number_of_factors = classed_eigenvalues["above_one"]["count"]
'''
Factor Interpretation
We calculate:
- Factor Scores
- Factor Loadings
'''
#Calculate the Factor loadings and variance
loadings, factorVariance = analyze(df, number_of_factors = number_of_factors, rotation="varimax")
#Add an index for better tabulation
prepared_loadings = addIndexToColouredList(convertToColouredList(loadings), list(df))
#Create headers for tabulation depending on the number of factors
headers=getHeadersDependingOnFactors(number_of_factors)
print(colored("- Factor Loadings Table", "blue"))
print(tabulate(prepared_loadings, headers=headers, tablefmt="fancy_grid"))
#Add the same index to the factor variance table
prepared_factorVariance = addIndexToColouredList(factorVariance, ["SS Loadings", "Proportion Var", "Cumulative Var"])
print(colored("- Factor Variance Table", "blue"))
print(tabulate(prepared_factorVariance, headers=headers, tablefmt="fancy_grid"))
#Calculate the Factor Scores matrix
factor_scores = get_Factor_Scores(df)
df_scores = pd.DataFrame(factor_scores)
print(tabulate(df_scores.head(),  tablefmt="fancy_grid"))