# Repository for the module Quantitative Research Methods

Currently set up for doing exploratory factor analysis:

![Sample Video](videos/terminal output.mov)

## Set-Up / Requirements:

- Python

- Pandas

  ```shell script
  pip install pandas
  ```

- Numpy

  ```shell script
  pip install numpy
  ```

- Matplotlib

  ```shell script
  pip install matplotlib
  ```

- Factor_analyzer

  - scipy
  - scikit-learn

  ```shell script
  pip install factor_analyzer
  ```

- tabulate

  ```shell script
  pip install tabulate
  ```

- termcolor

  ```shell script
  pip install termcolor
  ```

- colorama

  ```shell script
  pip install colorama
  ```

- halo

  ```shell script
  pip install halo
  ```

- Additional funtions that require to be added to the `factor_analyzer` module:

```python
def get_factor_coeffs(self, X):
    """
    Calculate the factor coefficients for the given data.
    """
        # check if the data is a data frame,
        # so we can convert it to an array
        if isinstance(X, pd.DataFrame):
            X = X.copy().values
        else:
            X = X.copy()

        # now check the array, and make sure it
        # meets all of our expected criteria
        X = check_array(X,
                        force_all_finite=True,
                        estimator=self,
                        copy=True)

        # meets all of our expected criteria
        check_is_fitted(self, 'loadings_')

        # see if we saved the original mean and std
        if self.mean_ is None or self.std_ is None:
            warnings.warn('Could not find original mean and standard deviation; using'
                          'the mean and standard deviation from the current data set.')
            mean = np.mean(X, axis=0)
            std = np.std(X, axis=0)
        else:
            mean = self.mean_
            std = self.std_

        # get the scaled data
        X_scale = (X - mean) / std

        # use the structure matrix, if it exists;
        # otherwise, just use the loadings matrix
        if self.structure_ is not None:
            structure = self.structure_
        else:
            structure = self.loadings_

        try:
            self.weights_ = np.linalg.solve(self.corr_, structure)
        except Exception as error:
            warnings.warn('Unable to calculate the factor score weights; '
                          'factor loadings used instead: {}'.format(error))
            self.weights_ = self.loadings_
        scores = np.dot(X_scale, self.weights_)
        return self.weights_
```

## Where to insert the dataset:

Please download the datasets you want to use this script on your own, the current file name which resembles the name of the dataset is: `data-final.csv`. It needs to be inserted in the `root` folder of this repository.
