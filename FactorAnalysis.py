from email import header
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import scipy.stats as stats
from colorama import init
from termcolor import colored
from halo import Halo
from factor_analyzer import FactorAnalyzer
from factor_analyzer.factor_analyzer import calculate_bartlett_sphericity
from tabulate import tabulate
'''
This python programm is supposed to check the suitability of data for a factor analysis according to Backhaus et al. (2018)
'''


#Helper functions basically to encapsulate the code
def get_classed_correlations(correlations):
    classes = [[0, 0.1], [0.1, 0.2], [0.2, 0.3], [0.3, 0.4], [0.4, 0.5], [0.5, 0.6], [0.6, 0.7], [0.7, 0.8], [0.8, 0.9], [0.9, 1]]
    #Extend neagtive correlation classes
    empty_classes = [0] * 10
    for count, val in enumerate(classes):
        for col in correlations: 
            for row in correlations:
                if abs(correlations[col][row]) > val[0] and abs(correlations[col][row]) < val[1] and col != row:
                    empty_classes[count] += 1
    print(empty_classes)
    stringed_classes = []
    for val in classes:
        stringed_classes.append(str(val))
    dict_classes = dict(zip(stringed_classes, empty_classes))
    return dict_classes     
def get_classed_Eigenvalues(eigenvalues):
    '''
    Expects a lis of eigenvalues and splits them into a list of list each containeing either eigenvalues above or below a certain threshold (1)
    First list is the eigenvalues above 1, second list is the eigenvalues below 1
    '''
    counter_above_one = 0
    counter_below_one = 0
    above_one = []
    below_one = []
    for eigenvalue in eigenvalues:
        if eigenvalue > 1:
            above_one.append(eigenvalue)
            counter_above_one += 1
        else:
            below_one.append(eigenvalue)
            counter_below_one += 1
    dictionary = {
        "above_one": {
            "complete_list": above_one,
            "count": counter_above_one,
        },
        "below_one": {
            "complete_list": below_one,
            "count": counter_below_one,
        }
    }
    return dictionary
def tabulate_Eigenvalues(ev):
    classed_eigenvalues = get_classed_Eigenvalues(ev)
    table = [["< 1", colored(classed_eigenvalues["below_one"]["count"], "red"),], ["> 1", colored(classed_eigenvalues["above_one"]["count"], "green")]]
    print(tabulate(table))

def get_Eigenvalues(df):
    spinner = Halo(text='Eigenvalues', spinner='dots', text_color='cyan')
    fa = FactorAnalyzer()
    fa.fit(df)
    spinner.start("Calculating Eigenvalues")
    # Check Eigenvalues
    ev, v = fa.get_eigenvalues()
    spinner.succeed("Eigenvalues were successfully calculated")
    return ev
def get_Communalities(df):
    spinner = Halo(text='Eigenvalues', spinner='dots', text_color='cyan')
    fa = FactorAnalyzer(bounds=(0.005, 1), impute='median', is_corr_matrix=False,
                method='minres', n_factors=8, rotation="varimax", rotation_kwargs={},
                use_smc=True)
    fa.fit(df)
    spinner.start("Calculating Communalities")
    # Check Eigenvalues
    comm = fa.get_communalities(rotation="varimax")
    spinner.succeed("Eigenvalues were successfully calculated")
    return comm

def get_Factor_Scores(df):
    spinner = Halo(text='Factor Scores', spinner='dots', text_color='cyan')
    
    fa = FactorAnalyzer(n_factors=8, rotation="varimax")
    fa.fit(df)
    spinner.start("Calculating Factor Scores")
    factor_scores = fa.transform(df)
    spinner.succeed("Factor Scores were successfully calculated")
    return factor_scores
def get_Factor_Coefficients(df):
    spinner = Halo(text='Factor Coefficients', spinner='dots', text_color='cyan')
    
    fa = FactorAnalyzer(n_factors=8, rotation="varimax")
    fa.fit(df)
    spinner.start("Calculating Factor Coeffecients")
    # Check Eigenvalues
    #print(fa)
    coeffocients = fa.get_factor_coeffs(df)
    spinner.succeed("Factor Coefficients were successfully calculated")
    return coeffocients

def plot_Eigenvalues(ev):
    '''
    Expects a list of eigenvalues and plots it
    '''
    should_we_plot_ev = input("Do you want to plot the eigenvalues? (y/n)")
    if should_we_plot_ev == "y" or should_we_plot_ev == "Y":
        plt.plot(ev, marker='o', markevery=1)
        plt.ylabel('Eigenvalue')
        plt.xlabel('Eigenvalue number')
        plt.xticks(list(range(1, len(ev))))
        plt.axhline(1, linestyle='--', color='green')
        plt.show()

def analyze(df, number_of_factors: int, rotation="varimax"):
    '''
    Returns both factor loadings as well as the factor variance as a tupel (loadings, variance), while expeting the dataframe, the number of factors and the rotation
    '''
    spinner = Halo(text='Calculating Factorvariance and loadings', spinner='dots', text_color='cyan')
    spinner.start()
    fa = FactorAnalyzer()
    if rotation == "none":
        fa.set_params(n_factors=number_of_factors)
    else:
        fa.set_params(n_factors=number_of_factors, rotation=rotation)
    fa.fit(df)
    loadings = fa.loadings_
    fVariance = fa.get_factor_variance()
    spinner.succeed("Factorvariance and loadings were successfully calculated")
    return (loadings, fVariance)


def check_correlation_kmo_bartlett(df, INDEX_OF_LAST_COLUMN):


    #Initialize Colorama
    init()

    #print(df.head(1))
    #Calculate correalation between every column of the dataframe, drop nan from df, include only numbers just to make sure 
    spinner = Halo(text='Calculating the Correlation Table', spinner='dots', text_color='cyan')
    spinner.start()
    correlations = df.corr()
    spinner.succeed("Correlation Table was successfully calculated")



    # Make sure everything is fine <=> Correlation matrix is symmetric
    number_of_values = correlations.shape[0]*correlations.shape[1]
    assert(number_of_values == INDEX_OF_LAST_COLUMN*INDEX_OF_LAST_COLUMN)
    print(f"The correlation table features {colored(number_of_values, 'cyan')} values.\n")

    # We now get the classed correlations in steps of 0.1 in between 0 and 1
    dict_classes = get_classed_correlations(correlations)


    # Lets print the results in the console using some pretty colors
    print(f"Correlation above {colored('1', 'white')} and below {colored('0.9', 'magenta')} => {dict_classes['[0.9, 1]']}")
    print(f"Correlation above {colored('0.9', 'magenta')} and below {colored('0.8', 'yellow')} => {dict_classes['[0.8, 0.9]']}")
    print(f"Correlation above {colored('0.8', 'yellow')} and below {colored('0.7', 'green')} => {dict_classes['[0.7, 0.8]']}")
    print(f"Correlation above {colored('0.7', 'yellow')} and below {colored('0.6', 'green')} => {dict_classes['[0.6, 0.7]']}")
    print(f"Correlation above {colored('0.6', 'green')} and below {colored('0.5', 'cyan')} => {dict_classes['[0.5, 0.6]']}")
    print(f"Correlation above {colored('0.5', 'cyan')} and below {colored('0.4', 'blue')} => {dict_classes['[0.4, 0.5]']}")
    print(f"Correlation above {colored('0.4', 'blue')} and below {colored('0.3', 'magenta')} => {dict_classes['[0.3, 0.4]']}")
    print(f"Correlation above {colored('0.3', 'magenta')} and below {colored('0.2', 'red')} => {dict_classes['[0.2, 0.3]']}")
    print(f"Correlation above {colored('0.2', 'red')} and below {colored('0.1', 'green')} => {dict_classes['[0.1, 0.2]']}")
    print(f"Correlation above {colored('0.1', 'green')} and below {colored('0', 'white')} => {dict_classes['[0, 0.1]']}")

    #This static funtion returns a tuple for the chi-square value and the p-value of the Bartlett-Test
    spinner.start("Checking Bartlett / Sphericity")
    chi_square_value, p_value = calculate_bartlett_sphericity(df)
    spinner.succeed("Bartlett / Sphericity was successfully calculated")
    print("BARTLETT Significance: ", colored(p_value, "green"))
    print("BARTLETT Chi-Square Value: ", colored(chi_square_value, "green"))

    #This static function returns the kmo value of the KMO Test
    from factor_analyzer.factor_analyzer import calculate_kmo
    spinner.start("Checking KMO")
    kmo_all,kmo_model=calculate_kmo(df)
    spinner.succeed("KMO was successfully calculated")
    print("KMO: ", colored(round(kmo_model, 2), "green"))

    

    #Lets now plot the existing classed correlation
    should_we_plot_correlation = input("Do you want to plot the correlation table? (y/n)")
    if should_we_plot_correlation == "y" or should_we_plot_correlation == "Y":
        print(colored("Plotting the correlation table", "cyan"))        
        fig, ax = plt.subplots()
        #Add x and y labels
        ax.set_xlabel('Correlation class')
        ax.set_ylabel('Amount of absolute correlations belonging to class')
        ax.bar(dict_classes.keys(), dict_classes.values(), color='blue')
        plt.show()

    return (kmo_all, kmo_model, chi_square_value, p_value, dict_classes)